package com.example.demo.models;

import com.sun.source.doctree.SerialDataTree;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.security.SecureRandomParameters;
import java.util.UUID;

@Entity
@Table(name = "app_auth_app")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppAuthApp implements Serializable {

    @Id
    @Column(name="id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "token")
    private String token;

    @Column(name = "is_staff_user")
    private Boolean is_staff_user;


}
