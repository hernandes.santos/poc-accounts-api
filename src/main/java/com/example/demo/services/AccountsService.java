package com.example.demo.services;

import com.example.demo.models.AppAuthApp;
import com.example.demo.repositories.AppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountsService {

   @Autowired
    private AppRepository appRepository;


   public AppAuthApp getApp(String token){
      AppAuthApp app = appRepository.findByToken(token);
      return app;
   }
}
