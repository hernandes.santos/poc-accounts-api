package com.example.demo.repositories;

import com.example.demo.models.AppAuthApp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AppRepository extends JpaRepository<AppAuthApp, UUID> {
    AppAuthApp findByToken(String token);
}
