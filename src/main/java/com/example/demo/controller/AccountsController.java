package com.example.demo.controller;

import com.example.demo.models.AppAuthApp;
import com.example.demo.models.DTO.IAS;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface AccountsController {
    @GetMapping("/ping")
    @Tag(name = "IAS")
    @Operation(summary = "Get health check", responses = {
            @ApiResponse(content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE,
                    schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "400", description = "Parameters not found") })
    public String getPing() ;

    @PostMapping("/v1/auth")
    @Tag(name = "IAS")
    @Operation(summary = "checkout token client", responses = {
            @ApiResponse(content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = AppAuthApp.class))),
            @ApiResponse(responseCode = "400", description = "Parameters not found") })
    public AppAuthApp authetication(@RequestBody IAS ias) ;
}
