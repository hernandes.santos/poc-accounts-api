package com.example.demo.controller;

import com.example.demo.models.AppAuthApp;
import com.example.demo.models.DTO.IAS;
import com.example.demo.services.AccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountsControllerImp implements AccountsController{

    @Value("${accounts_secret}")
    private String secretText;

    @Autowired
    private AccountsService accountsService;

    @GetMapping("/ping")
    public String getPing() {
        return "pingou"+secretText;
    }

    @Override
    public AppAuthApp authetication(IAS ias) {
        return accountsService.getApp(ias.getToken());
    }
}
