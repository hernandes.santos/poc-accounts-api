package com.example.pocaccountsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocAccountsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocAccountsApiApplication.class, args);
	}

}
