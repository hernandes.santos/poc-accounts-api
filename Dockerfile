FROM openjdk:11
VOLUME /tmp
ADD /target/poc-accounts-api-0.0.1-SNAPSHOT.jar poc-accounts-api.jar

ENTRYPOINT ["java","-jar", "/root/app/target/poc-accounts-api.jar"]
